const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  devServer:{
    proxy:{
      '/api': {
        target: `http://192.168.88.1/v1/admin/api`,
        changeOrigin: true,
        pathRewrite: {
          '^/api' : ''
        }
      }
    }
  },
  transpileDependencies: true
})
